/*
*
*   Import your dependencies or your own files
*   example:
*      -  import { gsap } from 'gsap';
*      -  import MyCustomClass from './folder/CustomClass';
*
*/

import jquery from 'jquery';
import Swiper, { Navigation, Pagination,EffectCreative,Parallax } from 'swiper';
import 'swiper/swiper.min.css';
import 'parsleyjs';
import 'parsleyjs/dist/i18n/fr.js';
import FormComponent from './form/FormComponent';
window.formComponent = new FormComponent();

import FormMessage from './form/formMessage.js';
window.formMessage = new FormMessage();





/***************************************************
██╗    ██╗██╗██████╗ ██╗ ██████╗ ██████╗ ███╗   ███╗
██║    ██║██║██╔══██╗██║██╔════╝██╔═══██╗████╗ ████║
██║ █╗ ██║██║██████╔╝██║██║     ██║   ██║██╔████╔██║
██║███╗██║██║██╔══██╗██║██║     ██║   ██║██║╚██╔╝██║
╚███╔███╔╝██║██████╔╝██║╚██████╗╚██████╔╝██║ ╚═╝ ██║
 ╚══╝╚══╝ ╚═╝╚═════╝ ╚═╝ ╚═════╝ ╚═════╝ ╚═╝     ╚═╝
***************************************************/
let styles = [
    'background: black'
    , 'color: white'
    , 'display: block'
    , 'padding: 10px 20px'
    , 'line-height: 40px'
    , 'text-align: center'
    , 'font-size: 12px'
    , 'font-familly: Arial,Helvetica,Sans-serif'
    , 'font-weight: bolder'
].join(';');

const swiper = new Swiper(".swiper-container", {
    modules: [Navigation, Pagination,EffectCreative,Parallax],
    loop:true,
    speed:600,
    slidesPerView:1,
    navigation: {
      nextEl: ".btn-next",
      prevEl: ".btn-prev",
    },
    parallax:true,
    pagination: {
        el: '.swiper-pagination',
    },
    grabCursor: true,
    breakpoints: {
      // when window width is >= 320px
      767: {
        slidesPerView: 3,
      },
    }
   

});

/*$('form').on('submit',function(e){
  e.preventDefault()
  $.ajax({
    method: 'POST',
    url: 'https://formsubmit.co/ajax/arnaud@wibicom.be',
    dataType: 'json',
    accepts: 'application/json',
    data: {
        name: "FormSubmit",
        message: "I'm from Devro LABS"
    },
    success: (data) => {
      console.log(data),
      $('.formSuccessMessage').show();
      $('input,textarea').val('')
    },
    error: (err) => console.log(err)
  });
})*/

/*
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
*/

 

 window.onload = function(){
    window.formComponent.initGlobal();
    window.formComponent.initView();
    window.formMessage.init();
  $('.intro').addClass('hide')
  $('body').on('scroll',function(){
    console.log('scroll');
    var containerHeight = $('.header').height() - $('header').outerHeight();
    var windowHeight = ( $(this).scrollTop() );
    if( windowHeight > containerHeight ){
     $("header").addClass('show-bg');
    } else {
     $('header').removeClass('show-bg');
    }
   });
 };
